FROM python:2.7-alpine
RUN apk add alpine-sdk linux-headers docker
COPY . /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
