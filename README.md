# GTFS Fetcher

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

>_GTFS Fetcher_ is a celery app that periodically fetches GTFS feeds from source registered on _FIWARE Orion Context Broker_ entities (using [GtfsTransitFeedFile](https://gitlab.com/synchronicity-iot/synchronicity-data-models/blob/master/UrbanMobility/GtfsTransitFeedFile/doc/spec.md) datamodel).

It provides a mechanism to fetch GTFS files (static timetables e related info), based on availability data of such files announced within the SynchroniCity framework, through GtfsTransitFeedFile type entities. The mentioned availability data is retrieved from the Context Broker via NGSI. It then handles the fetched GTFS in various ways, through plugins.

| ![GTFS Fetcher architecture](docs/gtfs_fetcher_arch.png) |
|:--:|
| GTFS Fetcher architecture |

In `task.py` we implement logic to iterate _Orion Context Broker_ queries and entities, and download feeds with new version. For all new feed versions we update container internal _**GTFS Version Database**_. In case at least one new GTFS version was downloaded, the task triggers sequentially the integration commands (plugins).

Plugins can be implemented in any programing language. It mus be executable, and be able to accept a list of file paths (decipting the location of the downloaded files).

For more information on the plugins, check the `plugins` directory.

# Configuration

### Celery specific configs
In the `celeryconfig.py` file, you can change some configurations, such as the AMPQ settings and the time intervals at which the script runs. The schedule can be changed (the syntax is somewhat similar to crontab).

### Environment variables
Either by running from docker container or command shell, ensure then the following environment variables are set:

- ORION_URL - URL of source Orion Context Broker
- FIWARE_SERVICEPATH - service path of source _Orion Context Broker_
- FIWARE_SERVICE - tenancy of source _Orion Context Broker_
- OUTPUT_FOLDER - container folder to store downloaded files
- ORION_ENTITY_PATTERNS - JSON array with _Orion Context Broker_ entities filters
- IGNORE_EXPIRED_GTFS - `false` value to always download GTFS feeds, even if expired
- PLUGIN_COMMAND_ARRAY - array of shell commands to run sequentially at end of all downloads that you can use to trigger feed integration

Note: if not running as docker and/or dont want to use environment variables, you may specifiy values for theres variables in the `constants.py` file.

# Plugins
_gtfs-fetcher_ will call the commands specified in the environment variable `PLUGIN_COMMAND_ARRAY`, mentioned above, after downloading at least one file.
Refer to documentation in `plugins/README.md`

# Deployment

### Docker and docker-compose
File `deployment/docker-compose.yml` can be used as an example to build and deploy the service.

   sudo docker-compose -f deployment/docker-compose.yml up -d

### Force execution
_gtfs-fetcher's_ orchestrator will launch the fetching task at a the defined time/date.
To force it running, the following command may be run:
  
    sudo docker-compose exec gtfs-fetcher python gtfs_fetcher_run_once.py

# Support
Any feedback on this documentation is highly welcome, including bugs, typos and suggestions.

The recommended way to provide feedback and receive support is to use repository [Issues](https://gitlab.com/synchronicity-iot/gtfs-fetcher/issues) and [Merge Requests](https://gitlab.com/synchronicity-iot/gtfs-fetcher/merge_requests).

# About 
For more information, check the project's site [here](https://synchronicity-iot.eu/)

# License
The NGSI Urban Mobility to GTFS service is licensed under Affero General Public License (GPL) version 3. See [See here](https://www.gnu.org/licenses/agpl-3.0.html).

The software is released as it is and we discharge any liability.
