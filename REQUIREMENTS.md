# GTFS Fetcher - Requirements

### GTFS Fetcher Orchestrator
Every day, at configured time, _GTFS Fetcher_ task _**fetches**_ entities filtered as configured from _Orion Context Broker_. For each entity retrieved the following steps are done:
- **GTFS Version Check**, by comparing `version` against container internal _**GTFS Version Database**_, ending entity processing if it is not a new version;
- **GTFS Download**, by retrieving GTFS zip file from `feedFileURL` into container internal file system;
In case at least one new GTFS version was downloaded, _GTFS Fetcher_ executes the integration step:
- _**GTFS Fetcher Plugin**_, by calling configured routine passing a list of all downloaded GTFS zip files as argument. The plugin should copy each file in list into service container and trigger service data update. As an example, for _Open Trip Planner_, the plugin should trigger graph build and, having success, should call **OTP API** to reload graph (in case autoReload is not configured).
