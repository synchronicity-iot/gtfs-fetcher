from __future__ import absolute_import

from celery.schedules import crontab
from redislite import Redis

rsl = Redis('/tmp/redis.db')
BROKER_URL = 'redis+socket://' + rsl.socket_file

CELERY_IMPORTS        = ("tasks",)
CELERY_RESULT_BACKEND = None
CELERY_SEND_EVENTS = False
CELERY_DEFAULT_QUEUE = 'scheduler_queue'

CELERYBEAT_SCHEDULE   = {
    'gtfs_fetcher': {
        'task': 'gtfs_fetcher',
        'schedule': crontab(hour=0, minute=0)
    },
}
