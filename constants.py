import os
import json

ORION_URL = os.environ.get('ORION_URL')
FIWARE_SERVICEPATH = os.environ.get('FIWARE_SERVICEPATH')
FIWARE_SERVICE = os.environ.get('FIWARE_SERVICE')

OUTPUT_FOLDER = os.environ.get('OUTPUT_FOLDER')
ORION_ENTITY_PATTERNS = os.environ.get('ORION_ENTITY_PATTERNS')
IGNORE_EXPIRED_GTFS = (os.environ.get('IGNORE_EXPIRED_GTFS') != "false")

PLUGIN_COMMAND_ARRAY = json.loads(os.environ.get('PLUGIN_COMMAND_ARRAY'))
