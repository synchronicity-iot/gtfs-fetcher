# _**GTFS Fetcher Plugins**_
For each plugin execution, _gtfs-fetcher_ passes one or many json structures, each encapsulated as strings, as follows:
                                gtfs\_fetcher\_entry = {
                                    'local\_path' : _/path/to/donloaded/file.zip,
                                    'ngsi\_entity' : { ... }
                                }

## Plugins
### Dummy Plugin
This is a simple plugin that does nothing but to print the received parameters.

### _Open Trip Planner_ integration example
`otp_plugin_example.py` file in `plugins/otp` folder is a python script example that can be configured to run by setting `PLUGIN_COMMAND_ARRAY`.

This is a simple script to trigger _Open Trip Planner_ graph build, after copying new feed files to _OTP_ graph directory. A sample `docker-compose.yml` is include in project to show how we can put it all together working.

_GTFS Fetcher_ container installs docker client in order to be able to instruct integration process available from linked containers.

### _Orion Context Broker_ Gtfs* entities integration example

This is a simple script to load GTFS data in to _Orion Context Broker_, by creating  Gtfs* (see the [datamodels](https://fiware-datamodels.readthedocs.io/en/latest/UrbanMobility/doc/introduction/index.html)).

`gtfs2orion.py` file in `plugins/gtfs2orion` folder is a python script example that can be configured to run by adding it to `PLUGIN_COMMAND_ARRAY`.

#### Disclaimer
At the moment, _gtfs-fetcher_ only supports the following GTFS-related entity types: GTFSAgency, GTFSService, GTFSStop, GTFSRoute, GTFSCalendarRule and GTFSCalendarDateRule

#### Plugin Configuration
The following environment variables *must* be set

- GTFS_FIELDS - provides a list with the entities to be uploaded to Orion (Refer to the Disclaimer Section in previous secion)
- ID_SUFIX    - Ids for the created entities will be in the format: "urn:ngsi-ld:<entity\_type>:<id\_sufix>:<identifier>". The "id\_sufix" may be specified in pairs, to match the source GtfsTransitFeedFile Id, as the following example:
 ID_SUFIX = { 'urn:ngsi-ld:GtfsTransitFeedFile:stcp:APD:STCP01':'porto:bus:stcp',
             'urn:ngsi-ld:GtfsTransitFeedFile:mdp:APD:MDP01':'porto:metro:stcp'}



