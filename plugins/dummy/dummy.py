#!/usr/bin/python2.7
# -*- coding: utf8 -*-

import os
import sys
import json

if __name__ == "__main__":
    if len(sys.argv)>1:
        for param in sys.argv[1:]:
            data=json.loads(param)
            for i in range(len(data)):
                filepath = data[i]['local_path']

                feed_id = data[i]['ngsi_entity']['id']
		print "New file downloaded at %s" % filepath
		print "NGSI source entity : %s" % data[i]['ngsi_entity']
                print
