#!/usr/bin/python2.7
# -*- coding: utf8 -*-
import os

GTFS2ORION_GTFS_ENTITIES = os.environ.get('GTFS2ORION_GTFS_ENTITIES')

GTFS2ORION_NGSI_ID_SUFFIX = os.environ.get('GTFS2ORION_NGSI_ID_SUFFIX')


# AMQ configs
HOST = 'rabbitmq'
USER       = "guest"
PASSWORD   = "guest"
VHOST      = "/"
AMQ_BROKER_URL = 'amqp://%s:%s@%s:5672/%s' % (USER, PASSWORD, HOST, VHOST)
