#!/usr/bin/python2.7
# -*- coding: utf8 -*-

from subprocess import Popen
import os
from datetime import datetime
from orionevent import OrionEntity

from kombu import Connection

from orion_gtfs_reader import get_gtfs
from orion_delete_entities import remove_all
import sys
from config_file import AMQ_BROKER_URL
from config_file import GTFS2ORION_GTFS_ENTITIES
from config_file import GTFS2ORION_NGSI_ID_SUFFIX
import json
import errno
import ast

def send_items(fetched_items, ngsi_id_sufix):
    """
        Sends the entities to Orion

        fetched_items: The list of items to send to Orion
        ngsi_id_suffix: The suffix that is used in the ID fields

        Doesn't return anything
    """
    sent = 0
    last_entity_type = ''
    for item in fetched_items:
        # Gets the entity with the correct data model and uploads it to Orion. The entities with ID are the only ones to be uploaded
        message = item.to_fiware_model(ngsi_id_sufix)
        if message.has_key('entity_id'):
            oe = OrionEntity(message)
            last_entity_type = oe.get_entity_type(message)
            oe.update_orion()
            # GtfsService is uploaded together with other entities so it is not counted because the number would not be correct
            # Counts the number of entities uploaded to Orion
            if last_entity_type != 'GtfsService':
                sent += 1
        else:
            print 'Message has no [entity_id]\n'
    print "\nType '{0}' entities loaded: {1}\n".format(last_entity_type, sent)


def zip2gtfs(zip_path, output_folder):
    """
        Unzips the given .zip file to the specified output folder

        zip_path: the path to the .zip file
        output_folder: the folder to unzip the files

        Doesn't return anything. It works directly with the files and folders
    """

    dir_list = os.listdir(output_folder)
    
    # Removes already existent gtfs files
    for item in dir_list:
        if item.endswith(".txt"):
            os.remove(os.path.join(output_folder, item))

    Popen(['unzip', zip_path, '-d',output_folder]).wait()
    return



def upload_orion_entities(output_folder, ngsi_id_suffix, feed_description):
    """
        Assembles the gtfs entities and sends the entities to Orion

        output folder: the output folder where to get the gtfs files
        ngsi_id_suffix: the needed id suffix to the entities' ID creation
        feed_description: the description to be used as name to the GtfsService entities

        Doesn't need to return anything
    """
    print '\nGetting GTFS entities from source...\n'
    with Connection(AMQ_BROKER_URL) as conn: 

        # Checks which entities will be uploaded
        if 'Agency' in GTFS2ORION_GTFS_ENTITIES:
            print "[agency]",
            # Gets the gtfs files
            fetched_items = get_gtfs('agency', output_folder=output_folder)

            # Gets the Agency ID so it can be referenced by other entities
            agencyID = fetched_items[0].to_fiware_model(ngsi_id_suffix)['entity_id']
            
            # Sends the entities to Orion
            send_items(fetched_items, ngsi_id_suffix)

        if 'Stop' in GTFS2ORION_GTFS_ENTITIES:            
            print "[stops]",
            fetched_items = get_gtfs('stops', agency_id=agencyID, output_folder=output_folder)
            send_items(fetched_items, ngsi_id_suffix)

        if 'CalendarRule' in GTFS2ORION_GTFS_ENTITIES:
            print "[calendar]",
            fetched_items = get_gtfs('calendar', agency_id=agencyID, output_folder=output_folder, feed_description=feed_description)
            send_items(fetched_items, ngsi_id_suffix)

        if 'CalendarDateRule' in GTFS2ORION_GTFS_ENTITIES:
            print "[calendar_dates]",
            fetched_items = get_gtfs('calendar_dates', agency_id=agencyID, output_folder=output_folder, feed_description=feed_description)
            send_items(fetched_items, ngsi_id_suffix)

        if 'Route' in GTFS2ORION_GTFS_ENTITIES:
            print "[routes]",
            fetched_items = get_gtfs('routes', agency_id=agencyID, output_folder=output_folder)
            send_items(fetched_items, ngsi_id_suffix)

    return


if __name__ == "__main__":

    """
        Main structure of the plugin.
        It sets variables and the output folder to be used in the execution of the plugin so it can then upload the entities to Orion.

    """

    if len(sys.argv)>1:

        PLUGIN_OUTPUT_FOLDER = os.environ.get('OUTPUT_FOLDER') + '/gtfs2ngsi_output' # Gets the path to the output folder where the zip file and the gtfs files will stored

        print "GTFS2ORION_NGSI_ID_SUFFIX='%s'" % GTFS2ORION_NGSI_ID_SUFFIX
        print "GTFS2ORION_GTFS_ENTITIES='%s'" % GTFS2ORION_GTFS_ENTITIES
        print "PLUGIN_OUTPUT_FOLDER='%s'" % PLUGIN_OUTPUT_FOLDER
        print

        try:
            # Loads the config variables
            GTFS2ORION_NGSI_ID_SUFFIX = json.loads(GTFS2ORION_NGSI_ID_SUFFIX.strip('\''))
            GTFS2ORION_GTFS_ENTITIES = ast.literal_eval(GTFS2ORION_GTFS_ENTITIES)
        except Exception as e:
                    print "Something went wrong loading configs: '%s'" % e
                    print "Exiting..."
                    sys.exit()

        try:
            # Creates the output folder
            os.makedirs(PLUGIN_OUTPUT_FOLDER)
        except OSError as e:
            if e.errno != errno.EEXIST:
                    print "Something went wrong creating dir '%s'" % PLUGIN_OUTPUT_FOLDER
                    print "Exiting..."
                    sys.exit()
        
        for param in sys.argv[1:]:
            data=json.loads(param) 
            
            # Runs the plugin for each GtfsTransitFeedFile enity
            for index in range(len(data)):
                # Gets the path to the .zip gtfs file
                filepath = data[index]['local_path']
 
                # Gets the entity ID and description so they can be used in the data models
                feed_id = data[index]['ngsi_entity']['id']
                feed_description = data[index]['ngsi_entity']['description']['value']

                # Gets the suffix needed for the ID by checking it in the config dictionary
                if feed_id in GTFS2ORION_NGSI_ID_SUFFIX:
                        ngsi_id_suffix = GTFS2ORION_NGSI_ID_SUFFIX[feed_id]
                else:
                        print "Something went wrong processing GtfsTransitFeedFile entity '%s'. No configured suffix was found." % feed_id
                        print "Skipping..."
                        continue

                # Unzip gtfs 
                zip2gtfs('output/{0}'.format(filepath), PLUGIN_OUTPUT_FOLDER)
                Popen(['ls', PLUGIN_OUTPUT_FOLDER]).wait()

                # before uploading new 
                # FIX ME: all the entities are being deleted, regardless the existence of new gtfs files 
                # Deletes only the entities of the same type of the ones to be uploaded
                for gtfs_entity_type in GTFS2ORION_GTFS_ENTITIES:
                    print "Removing '%s' entities" % gtfs_entity_type
                    remove_all(gtfs_entity_type, ngsi_id_suffix)

                # Calls the function to upload the entities to Orion
                upload_orion_entities(PLUGIN_OUTPUT_FOLDER, ngsi_id_suffix, feed_description)
