import requests

import threading
import os
from traceback import print_exc

page_size = 1000
NUM_PROCESSES = 8
ORION_URL = os.environ.get('ORION_URL')

headers = {
    # 'fiware-service': 'service',
    # 'fiware-servicepath': '/subservice',
    # 'x-auth-token': 'token'
}

def get_entities_count(gtfs_type, ngsi_id_suffix):
    """
    Return the total number of entities

    :return: number of entities (or -1 if there was some error)
    """

    ngsi_type = 'Gtfs' + gtfs_type
    ngsi_id = 'urn:ngsi-ld:' + ngsi_type + ':' + ngsi_id_suffix + ':*'

    # Trick: asking for a non existing attribute (attrs=noexist) will make response very small (only entity id/type)
    res = requests.get(ORION_URL + '/v2/entities?limit=1&options=count&attrs=noexist&type=' + ngsi_type +"&idPattern=" + ngsi_id, headers=headers)
    if res.status_code != 200:
        print 'Error getting entities count (%d): %s' % (res.status_code, res.json())
        return -1
    else:
        return int(res.headers['fiware-total-count'])


def initial_statistics(gtfs_type, ngsi_id_suffix):
    """
    Print some initial data statistics
    """
    total = get_entities_count(gtfs_type, ngsi_id_suffix)
    print 'There are %d entities' % total
    pages = total / page_size
    rest = total - (pages * page_size)
    print 'There are %d pages of %d entities (and a final page of %d entities)' % (pages, page_size, rest)


def remove_page(gtfs_type, ngsi_id_suffix, offset):
    """
    Removes the first page of entities
    :return: True if removal was ok, False otherwise
    """

    ngsi_type = 'Gtfs' + gtfs_type
    ngsi_id = 'urn:ngsi-ld:' + ngsi_type + ':' + ngsi_id_suffix + ':*'

    try:
        # Trick: asking for a non existing attribute (attrs=noexist) will make response very small (only entity id/type)
        res = requests.get(ORION_URL + '/v2/entities?attrs=noexist&offset=' + str(offset) + '&limit=' + str(page_size) + '&type=' + ngsi_type + "&idPattern=" + ngsi_id, headers=headers)
        if res.status_code != 200:
            return False

        entities = []
        for entity in res.json():
            entities.append({'id': entity['id'], 'type': entity['type']})

        body = {
            'entities': entities,
            'actionType': 'DELETE'
        }

        res = requests.post(ORION_URL + '/v2/op/update', json=body, headers=headers)
        if res.status_code != 204:
            print 'Error deleting entities (%d): %s' % (res.status_code, res.json())
            return False

        return True
    except Exception:
        print_exc()
        return False

class FuncThread(threading.Thread):
    def __init__(self, target, *args):
        self._target = target
        self._args = args
        threading.Thread.__init__(self)

    def run(self):
        self._target(*self._args)


def remove_all(gtfs_type, ngsi_id_suffix):
    """
    Remove all entities, page after page
    """
    i = 1
    thread_pool = []
    to_remove = get_entities_count(gtfs_type, ngsi_id_suffix)
#    return
    while to_remove > 0:
        num_threads = int(to_remove / 1000)
        if to_remove % 1000 > 0:
            num_threads = num_threads + 1
        if num_threads > NUM_PROCESSES:
            num_threads = NUM_PROCESSES
        for proc_num in list(reversed(range(num_threads))):
            print '- Remove page %d' % i
            new_thread = FuncThread(remove_page, gtfs_type, ngsi_id_suffix, proc_num * page_size)
            new_thread.start()
            thread_pool.append(new_thread)
            i += 1
        for thread in thread_pool:
            thread.join()
        thread_pool = []
        to_remove = get_entities_count(gtfs_type, ngsi_id_suffix)
