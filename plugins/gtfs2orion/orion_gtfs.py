# encoding: utf-8

import sys
from geomet import wkt
import json
from datetime import datetime
import urllib

class GenericObject(object):
    """
        A generic object that includes all the methods and properties that every Orion class will use
    """

    as_dict = None

    def set_geometry(self):
        """
            Sets the geometry of the entity to WKT standards with lat and long.

            return: None, if there isn't any geometry field. Otherwise it changes the geomery attribute
        """

        geometry = getattr(self, 'geometry', None)
        if geometry:
            self.geometry = wkt.loads(geometry)
        else:
            return None

    def __init__(self, **kwargs):
        """
            Sets the received item into a dictionary
        """
        for key, value in kwargs.iteritems():
            # if isinstance(value, dict):
            #     _finditem(value, ['spa', 'eng', 'por'])

            self.__setattr__(key, value)

        self.set_geometry()
        self.as_dict = self.__dict__

    @staticmethod
    def urlencode_strings(_object):
        """
            Encodes a query string to be used to send the items to Orion

            _object: The entity that will be uploaded

            return: The encoded string to query Orion
        """
        if _object is None:
            return None
        if isinstance(_object, basestring):
            decomposed_url = _object.split('?')
            if len(decomposed_url) > 1:
                try:
                    return decomposed_url[0] + '?' + urllib.quote_plus(decomposed_url[1])
                except:
                    return decomposed_url[0] + '?' + urllib.quote_plus(decomposed_url[1].encode('latin9'))
            return decomposed_url[0]
        elif isinstance(_object, dict):
            return {key: GenericObject.urlencode_strings(value) for key, value in _object.iteritems()}
        elif isinstance(_object, list):
            return [GenericObject.urlencode_strings(item) for item in _object]

    @staticmethod
    def strip_bad_characters(field):
        """
            Takes out the not allowed characters from a given string

            field: The string to take out the characters

            return: String with the allowed characters
        """
        try:
            if field and (not isinstance(field, dict)):
                field = field.replace('\r\n', ' ')
                field = field.replace('\n', ' ')
                field = field.replace('\r', ' ')
                field = field.replace('\t', ' ')
                field = field.replace('<', ' ')
                field = field.replace('>', ' ')
                field = field.replace('=', ' ')
                field = field.replace(';', ' ')
                field = field.replace('(', ' ')
                field = field.replace(')', ' ')
                field = field.replace('\"', ' ')
                field = field.replace('\'', ' ')
                field = field.replace('\\', '\\\\')
                return field.strip()
            else:
                return field
        except:
            return field


class Agency(GenericObject):

    def to_fiware_model(self, id_sufix):
        """
            Converts the given item to a complaint class with the correct data model
            
            id_sufix: The id suffix to be used in the id of the entity

            return: An entity with the correct data model
        """
        as_dict = self.__dict__
        name = GenericObject.strip_bad_characters(as_dict.get('agency_name'))
        fiware_model = {
            'name':                 name,
            'source':               GenericObject.urlencode_strings(as_dict.get('agency_url', None)),
            'timezone':             as_dict.get('agency_timezone', None),
            'language':             as_dict.get('agency_lang', None),
            'entity_type':           'GtfsAgency',
            'entity_id':             'urn:ngsi-ld:GtfsAgency:' + id_sufix + ':' + as_dict.get('agency_id')
        }
        return fiware_model


class Stop(GenericObject):
    def set_geometry(self):
        """
            Sets the geometry to a format to be correctly identified by Orion

            It changes the given geometry field
        """
        stop_lat = getattr(self, 'stop_lat', None)
        stop_lon = getattr(self, 'stop_lon', None)

        if stop_lat and stop_lon:
            try:
                lat = float(stop_lat)
                lon = float(stop_lon)
            except:
                lat = lon = None
            
            # Checks if the values are in a correct range
            if lat and lon and lat > -180 and lat <= 180 and lon > -180 and lon <= 180:
                self.geometry = wkt.loads('POINT({} {})'.format(lon, lat))
        else:
            return None

    def to_fiware_model(self, id_sufix):
        """
            Converts the given item to a complaint class with the correct data model
            
            id_sufix: The id suffix to be used in the id of the entity

            return: An entity with the correct data model
        """
        as_dict = self.__dict__
        name = GenericObject.strip_bad_characters(as_dict.get('stop_name'))
        fiware_model = {
            'code':                 as_dict.get('stop_code', None),
            'name':                 name,
            'operatedBy':           [as_dict.get('agency_id', None)],
            'page':                 GenericObject.urlencode_strings(as_dict.get('stop_url', None)),
            'zoneCode':             as_dict.get('zone_id', None),
            "location":             as_dict.get('geometry', None),
            "entity_type":          "GtfsStop",
            "entity_id":            'urn:ngsi-ld:GtfsStop:' + id_sufix + ':' + as_dict.get('stop_id')
        }
        return fiware_model


class Service(GenericObject):

    def to_fiware_model(self, id_sufix):
        """
            Converts the given item to a complaint class with the correct data model
            
            id_sufix: The id suffix to be used in the id of the entity

            return: An entity with the correct data model
        """
        as_dict = self.__dict__
        fiware_model = {
            'code':                 as_dict.get('service_id', None),
            'operatedBy':           as_dict.get('agency_id', None),
            "entity_type":           "GtfsService",
            "entity_id":             'urn:ngsi-ld:GtfsService:' + id_sufix + ':' + as_dict.get('service_id'),
            'name':                 as_dict.get('description')
        }
        return fiware_model


class CalendarRule(GenericObject):

    def to_fiware_model(self, id_sufix):
        """
            Converts the given item to a complaint class with the correct data model
            
            id_sufix: The id suffix to be used in the id of the entity

            return: An entity with the correct data model
        """
        as_dict = self.__dict__
        startDate = as_dict.get('start_date', None)

        # Puts the Date fields with the correct format
        if startDate:
            startDate = startDate[0:4] + '-' + startDate[4:6] + '-' + startDate[6:8]
        endDate = as_dict.get('end_date', None)
        if endDate:
            endDate = endDate[0:4] + '-' + endDate[4:6] + '-' + endDate[6:8]
        
        fiware_model = {
            'hasService':           'urn:ngsi-ld:GtfsService:' + id_sufix + ':' + as_dict.get('service_id', 'unknown'),
            'monday':               (as_dict.get('monday', None) == '1'),
            'tuesday':              (as_dict.get('tuesday', None) == '1'),
            'wednesday':            (as_dict.get('wednesday', None) == '1'),
            'thursday':             (as_dict.get('thursday', None) == '1'),
            'friday':               (as_dict.get('friday', None) == '1'),
            'saturday':             (as_dict.get('saturday', None) == '1'),
            'sunday':               (as_dict.get('sunday', None) == '1'),
            'startDate':            startDate,
            'endDate':              endDate,
            "entity_type":           "GtfsCalendarRule",
            "entity_id":             'urn:ngsi-ld:GtfsCalendarRule:' + id_sufix + ':' + \
                                        as_dict.get('service_id') + \
                                        startDate + \
                                        endDate
        }
        return fiware_model


class CalendarDateRule(GenericObject):

    def to_fiware_model(self, id_sufix):
        """
            Converts the given item to a complaint class with the correct data model
            
            id_sufix: The id suffix to be used in the id of the entity

            return: An entity with the correct data model
        """
        as_dict = self.__dict__
        
        # Puts the Date field with the correct format
        appliesOn = as_dict.get('date', None)
        exceptionType = as_dict.get('exception_type', None)
        ID = as_dict.get('service_id', None)

        if appliesOn and exceptionType and ID:
            # Puts the Date field with the correct format
            appliesOn = appliesOn[0:4] + '-' + appliesOn[4:6] + '-' + appliesOn[6:8]

            fiware_model = {
                'hasService':           'urn:ngsi-ld:GtfsService:' + id_sufix + ':' + as_dict.get('service_id', 'unknown'),
                'appliesOn':            appliesOn,
                'exceptionType':        exceptionType,
                "entity_type":          "GtfsCalendarDateRule",
                "entity_id":            'urn:ngsi-ld:GtfsCalendarDateRule:' + id_sufix + ':' + ID + appliesOn
            }
            return fiware_model
        else:
            return {}


class Route(GenericObject):

    def to_fiware_model(self, id_sufix):
        """
            Converts the given item to a complaint class with the correct data model
            
            id_sufix: The id suffix to be used in the id of the entity

            return: An entity with the correct data model
        """
        as_dict = self.__dict__
        name = GenericObject.strip_bad_characters(as_dict.get('route_long_name'))
        description = GenericObject.strip_bad_characters(as_dict.get('route_desc'))
        fiware_model = {
            'shortName':            as_dict.get('route_short_name', None),
            'name':                 name,
            'description':          description,
            'operatedBy':           as_dict.get('agency_id', None),
            'routeType':            as_dict.get('route_type', None),
            'routeID':              as_dict.get('route_id', None),
            'page':                 GenericObject.urlencode_strings(as_dict.get('route_url', None)),
            'routeColor':           as_dict.get('route_color', None),
            'routeTextColor':       as_dict.get('route_text_color', None),
            "entity_type":           "GtfsRoute",
            "entity_id":             'urn:ngsi-ld:GtfsRoute:' + id_sufix + ':' + as_dict.get('route_id')
        }
        return fiware_model

#
# DISCLAIMER: The following models are still not supported (WIP)
#

class Shape(dict):
     def set_geometry(self):
         return None
 
class Trip(GenericObject):
    # linestring = None

    # def set_location(self, value):
    #     self.linestring = value

    def to_fiware_model(self, id_sufix):
        """
            Converts the given item to a complaint class with the correct data model
            
            id_sufix: The id suffix to be used in the id of the entity

            return: An entity with the correct data model
        """
        as_dict = self.__dict__
        headSign = GenericObject.strip_bad_characters(as_dict.get('trip_headsign'))
        fiware_model = {
            'headSign':             headSign,
            'direction':            as_dict.get('direction_id', None),
            'block':                as_dict.get('block_id', None),
            'hasService':           'urn:ngsi-ld:GtfsService:' + id_sufix + ':' + as_dict.get('service_id', 'unknown'),
            'hasRoute':             'urn:ngsi-ld:GtfsRoute:' + id_sufix + ':' + as_dict.get('route_id', 'unknown'),
            'wheelChairAccessible': as_dict.get('wheelchair_accessible', None),
            'entity_type':          'GtfsTrip',
            'entity_id':            'urn:ngsi-ld:GtfsTrip:' + id_sufix + ':' + as_dict.get('trip_id'),
            'hasShape':             'urn:ngsi-ld:GtfsRoute:' + id_sufix + ':' + as_dict.get('shape_id', 'unknown'),
        }
        # if self.linestring:
        #     fiware_model['location'] = self.linestring
        return fiware_model


class StopTime(GenericObject):

    def to_fiware_model(self, id_sufix):
        """
            Converts the given item to a complaint class with the correct data model
            
            id_sufix: The id suffix to be used in the id of the entity

            return: An entity with the correct data model
        """
        as_dict = self.__dict__
        headSign = GenericObject.strip_bad_characters(as_dict.get('stop_headsign'))
        arrival_time = as_dict.get('arrival_time', None).split(':')
        departure_time = as_dict.get('departure_time', None).split(':')
        fiware_model = {
            'hasStop':              'urn:ngsi-ld:GtfsStop:' + id_sufix + ':' + as_dict.get('stop_id', 'unknown'),
            'hasTrip':              'urn:ngsi-ld:GtfsTrip:' + id_sufix + ':' + as_dict.get('trip_id', 'unknown'),
            'stopHeadsign':         headSign,
            'stopSequence':         as_dict.get('stop_sequence', None),
            'arrivalTime':          '{0:02d}:{1:02d}:{2:02d}'.format(*map(int, arrival_time)),
            'departureTime':        '{0:02d}:{1:02d}:{2:02d}'.format(*map(int, departure_time)),
            'entity_type':          'GtfsStopTime',
            'entity_id':            'urn:ngsi-ld:GtfsStopTime:' + id_sufix + ':' + \
                                    as_dict.get('trip_id') + '_' + \
                                    as_dict.get('stop_sequence')
        }
        return fiware_model
