#!/usr/bin/python2.7
# -*- coding: utf8 -*-

import csv
from orion_gtfs import Agency
from orion_gtfs import Stop
from orion_gtfs import Service
from orion_gtfs import CalendarRule
from orion_gtfs import CalendarDateRule
from orion_gtfs import Route
from orion_gtfs import Shape    # per each route (memory only)
from orion_gtfs import Trip     # per each route (get shape from memory)
from orion_gtfs import StopTime # per each route
import os

def appendByType(target, provider_type, provider_item, agency_id, feed_description):
    """
        Appends, to a target list, the items in a class with the correct data model to be uploaded to Orion

        target: The list to append items
        provider_type: The type of Gtfs to be appended
        provider_item: The item to be appended
        agency_id: The agency_id to which that item belongs to
        feed_description: The GtfsTransitFeedFile description to be used in the GtfsService

        Doesn't return anything, it transforms the provided list
    """

    # Checks which Gtfs type to add
    if provider_type == 'agency':
        # Adds the item with the correct class
        target.append(Agency(**provider_item))
    
    elif provider_type == 'stops':
        # Adds the agency_id to the item so it can be complaint with the data model
        provider_item['agency_id'] = agency_id
        target.append(Stop(**provider_item))
    
    elif provider_type == 'calendar':
        
        provider_item['agency_id'] = agency_id
        
        # Adds the description to the item so it can create the GtfsService entity
        provider_item['description'] = feed_description
        target.append(Service(**provider_item))
        target.append(CalendarRule(**provider_item))
    
    elif provider_type == 'calendar_dates':
        provider_item['agency_id'] = agency_id
        provider_item['description'] = feed_description
        target.append(Service(**provider_item))
        target.append(CalendarDateRule(**provider_item))
    
    elif provider_type == 'routes':
        provider_item['agency_id'] = agency_id
        target.append(Route(**provider_item))
    
    elif provider_type == 'shapes':
        target.append(Shape(**provider_item))
    
    elif provider_type == 'trips':
        target.append(Trip(**provider_item))
    
    elif provider_type == 'stop_times':
        target.append(StopTime(**provider_item))


def get_gtfs(gtfs_type, feed_description=None, agency_id=None, output_folder='output'):
    """
        Gets the gtfs file and transforms the results from each file into the data model, appending them to result list
    
        gtfs_type: The gtfs file type to open
        feed_description: The description from GtfsTransitFeedFile to be used in the name of GtfsService
        agency_id: The agency_id that references to a GtfsAgency entity
        output_folder: The output folder where the files are located

        return: A list with Gtfs entities with the correct data model
    """
    filepath = output_folder + '/' + gtfs_type + '.txt'
    result = []
    if os.path.exists(filepath):
        data = list(csv.reader(open(filepath)))
        
        # Adds each item in the files to a list of dictionaries
        csv_dict_list = [{a:b for a, b in zip(data[0], i)} for i in data[1:]]
        
        # Checks if csv_dict_list is a list or a dict, since if there is only 1 item, Python doesn't make a list with 1 item
        if isinstance(csv_dict_list,list):
        
            for item in csv_dict_list:
        
                # Appends to result the items converted to their respective data model        
                appendByType(result, gtfs_type, item, agency_id, feed_description)
        
        elif isinstance(csv_dict_list, dict):
            appendByType(result, gtfs_type, csv_dict_list, agency_id, feed_description)
    
    return result
