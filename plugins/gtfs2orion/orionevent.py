from datetime import datetime
import json
import requests
from traceback import format_exc
import os

MIME_JSON = 'application/json; charset=utf-8'
ORION_URL = os.environ.get('ORION_URL')

class OrionEntity:
    """
        Object created to upload the entities to Orion
    """
    id = ''  # Entity id
    entity_type = ''
    body = {}
    attributes = {}
    orion_request_ok = False

    def __init__(self, body=body):
        
        self.body = body
        self.attributes = self.get_attributes(body)
        self.id = self.get_id(body)
        self.entity_type = self.get_entity_type(body)
        self.get_coords()

    def get_coords(self):
        """
            Sets the location field to the right format so Orion can recognize it

            Doesn't return anything, it changes directly the location field
        """
        lat = self.body.get('latitude', None)
        lon = self.body.get('longitude', None)
        location = self.body.get('location', None)

        # Checks if location field exists and then sets it to the format accepted by Orion
        if location and isinstance(location, dict) and not (lat and lon):
            location['crs'] = {'type':'name','properties':{'name':'EPSG:4326'}}
            self.attributes['location'] = {
                'value': location,
                'type': 'geo:json',
            }

    def get_type(self, value, key):
        """
            This function gives a type to every field depending on its content

            value: The value of the field
            key: The key of the field, needed to distinguish the field 'location' from other dictionaries

            return: pair of 'value' and 'type'
        """
        var_type = type(value)

        if var_type is int or var_type is float:
            return value, 'Number'

        elif var_type is bool:
            return value, 'Boolean'

        # Assume it's a string: may be a string, date or location
        elif var_type is list or var_type is dict:
            if key == 'location':
                return value, 'geo:json'
            elif key == 'operatedBy':
                return value, 'array'
            return value, 'StructuredValue'
        
        # Checks different datetime possible formats
        else:
            try:
                datetime.strptime(value, '%Y-%m-%d %H:%M:%S')
            except:
                try:
                    datetime.strptime(value, '%Y-%m-%dT%H:%M:%S.%fZ')
                except:
                    try:
                        datetime.strptime(value, '%Y-%m-%dT%H:%M:%SZ')
                    except:
                        return value, 'Text'

            return value, 'DateTime'

    def get_attributes(self, body):
        """
            Constructs the dictionary with the value and type that will be associated to the key.
            Orion needs the value and the type so it can recognize and process it

            body: The dictionary of the entity

            return: The entity with the dictionary with value and type in each field 
        """

        attributes = {}
        for key, value in body.items():
            # Checks for the keys that don't need to have the type declared
            if key == 'entity_type' or key == 'entity_id' or key == 'retries':
                continue


            value, type = self.get_type(value, key)
            attribute = {'value': value, 'type': type}

            attributes[key] = attribute

        #Calls the function that processes the location field
        self.get_coords()

        return attributes

    def get_id(self, body):
        self.attributes['id'] = str(body.get('entity_id'))
        return body.get('entity_id')

    def get_entity_type(self, body):
        self.attributes['type'] = body.get('entity_type')
        return body.get('entity_type')

    def get_entity(self):
        return self.attributes

    def update_orion(self):
        """
            Creates and updates the entities in Orion

            Doesn't return anything. It queries directly Orion
        """
        entity = self.get_entity()
        # Sets the required parameters to make the query to Orion
        payload = {
            'actionType': 'append',
            'entities': [entity]
        }
        payload_raw = json.dumps(payload)
        headers = {
            'Content-Type': MIME_JSON,
            'Content-Length': str(len(payload_raw))
            # ,
            # 'Fiware-Service': FIWARE_SERVICpyt E,
            # 'Fiware-Servicepath': FIWARE_SPATH
        }
        try:
            req = requests.post(
                ORION_URL + '/v2/op/update',
                data=payload_raw,
                headers=headers,
            )
            # Checks Orion response. Orion has a range in answer codes in which it confirms if the request was OK or not
            if 200 <= req.status_code < 300:
                self.orion_request_ok = True
            else:
                self.orion_request_ok = False
                print 'Status error [{0}] updating Orion entity id [{1}]'.format(
                    req.status_code,
                    entity.get('id', 'unknown')
                )
                print req.text
        except:
            print 'Exception updating Orion entity id [{0}]'.format(entity.get('id', 'unknown'))
            print format_exc()
