#!/usr/bin/python2.7
# -*- coding: utf8 -*-

from traceback import format_exc
from subprocess import Popen
import shutil
import os
import sys
import errno
import json

"""
import requests
def otp_reload():
    try:
        r = requests.put(OTP_URL+'/otp/routers', verify=False, auth=HTTPBasicAuth('ROUTERS', 'ultra_secret'))
        print r.content
        return True
    except:
        print 'Exception trying to instruct OTP to reloud graph'
        print format_exc()
        return False
"""

OTP_FOLDER = '/code/otp-build-files'

def otp_build():
    try:
        Popen('docker exec -ti deployment_otp_1 java -Xmx3G -Duser.timezone=Europe/Lisbon -jar ./otp-shaded.jar --build /opt/opentripplanner/graphs/default'.split(' ')).wait()
        return True
    except:
        print 'Exception trying to build OTP graph'
        print format_exc()
        return False


if __name__ == "__main__":

    if len(sys.argv)>1:
        for param in sys.argv[1:]:
            data=json.loads(param)

            OUTPUT_FOLDER = os.environ.get('OUTPUT_FOLDER')

            try:
                os.makedirs(OTP_FOLDER)
            except OSError as e:
                if e.errno != errno.EEXIST:
                        print "Something went wrong creating dir '%s'" % OTP_FOLDER
                        print "Exiting..."
                        sys.exit()


            for i in range(len(data)):
                filepath = data[i]['local_path'] 
                shutil.copy('{0}/{1}'.format(OUTPUT_FOLDER,filepath), '{0}/{1}'.format(OTP_FOLDER, filepath))
                # remove_filepath = '{0}/{1}'.format(OTP_FOLDER, remove_filename)
                # if not remove_filepath is None and os.path.exists(remove_filepath):
                #     os.remove(remove_filepath)
                print 'Copied file:'
                print 'output/{0}'.format(filepath)
                print 'To:'
                print '{0}/{1}'.format(OTP_FOLDER, filepath)
                print '---'
        otp_build()
