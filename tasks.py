#!/usr/bin/python2.7
# -*- coding: utf8 -*-

from celery import shared_task
import shelve
from celery import Celery
from celeryconfig import BROKER_URL

import requests
import json
from traceback import format_exc

from datetime import datetime
from datetime import timedelta
from dateutil.parser import parse
import os, errno
from subprocess import call
import json

from constants import ORION_URL, FIWARE_SERVICEPATH, FIWARE_SERVICE, OUTPUT_FOLDER, ORION_ENTITY_PATTERNS, IGNORE_EXPIRED_GTFS, PLUGIN_COMMAND_ARRAY

celery = Celery('tasks', broker=BROKER_URL)

def file_fetcher(url, remove_filename):
    """ 
    Downloads a file on a specified utl

    :return: True/False depending if file was downloaded or not    
    """
    try:
        r = requests.get(url)
        filepath = '{0}/{1}'.format(OUTPUT_FOLDER, os.path.basename(url))
        if os.path.exists(filepath):
            os.remove(filepath)
        f = open(filepath, 'wb')
        f.write(r.content)
        f.close()
        return True
    except:
        print 'Exception trying to get GTFS file from {0}'.format(url)
        print format_exc()
        return False



def plugin_executer(plugin_params):
    """
    Runs each plugin sequencially
    """
    if len(plugin_params) > 0:
        # Call each plugin
        for cmd in PLUGIN_COMMAND_ARRAY:
            print
            print "----------------------------------------------------------------------------"
            print "Calling plugin '%s'" % cmd
            print "----------------------------------------------------------------------------"
            print
            cmd_args= cmd.split(' ')
            json_string = json.dumps(plugin_params)
            cmd_args.append(json_string)
            call(cmd_args)
        print "----------------------------------------------------------------------------"
        print "Finished plugins"
        print "----------------------------------------------------------------------------"
    else:
        print
        print "No plugins called since no new files were fetched."

@shared_task(name="gtfs_fetcher")
def gtfs_fetcher():
    """
    Celery task to fetch gtfs files from sources specified on GtfsTransitFeedFile entities.
    It allows the execution of plugins.

    """

    print "Starting task."
    plugin_params = []

    try:
        os.makedirs(OUTPUT_FOLDER)
    except OSError as e:
        if e.errno != errno.EEXIST:
		print "Something went wrong creating dir '%s'" % OUTPUT_FOLDER
		print "Exiting..."
		return

    print "Fetching entities from orion based on pattern: '%s'." % ORION_ENTITY_PATTERNS
    idPatterns = json.loads(ORION_ENTITY_PATTERNS)
    if isinstance(idPatterns, str) or isinstance(idPatterns, unicode):
        idPatterns = [idPatterns]
    if isinstance(idPatterns, list):

        # Load History for previous fetches ( to prevent downloading previous files again)
        db = shelve.open("gtfs-shelve")
        request_data = {
          'entities': []
        }

        # Prepare the request to Orion
        for idPattern in idPatterns:
            request_data['entities'].append({
                'idPattern': idPattern,
                'type': 'GtfsTransitFeedFile'
            })
        request_headers = {
            'Content-Type': 'application/json'
        }
        if not FIWARE_SERVICE is None and FIWARE_SERVICE.strip()!='':
            request_headers['Fiware-Service'] = FIWARE_SERVICE
        if not FIWARE_SERVICEPATH is None and FIWARE_SERVICEPATH.strip()!='':
            request_headers['Fiware-ServicePath'] = FIWARE_SERVICEPATH

               
        try:
            # Make the request
            response = requests.post(
                ORION_URL + '/v2/op/query',
                data = json.dumps(request_data),
                headers = request_headers
            )


            feed_version = ""
            if 200 <= response.status_code < 300:
                entities = json.loads(response.content)
                if entities is not None:
                    print "Fetched %d entities from orion." % len(entities)
                    if not isinstance(entities,list):
                        entities = [entities]

                    # Processes retrieved Entities 
                    for entity in entities:
                        # print entity
                        feed_version = str(entity[u'version'][u'value'][u'id'])
                        entity_id = str(entity[u'id'])
                        feed_url = str(entity[u'feedFileURL'][u'value'])
                        feed_end_date = None
                        filename = os.path.basename(feed_url)

                        if entity.__contains__('endDate'):
                            feed_end_date = parse(str(entity[u'endDate'][u'value'])).date
                        else:
                            feed_end_date = datetime.now().date

                        # checks if gtfs data as expired (based on 'endDate' attribute)
                        if (feed_end_date < datetime.now().date and IGNORE_EXPIRED_GTFS) \
                        or db.get(entity_id) is None \
                        or db.get(entity_id).get('last_known_version', '???') != feed_version:
                            previous_version_filename = None
                            if not db.get(entity_id) is None:
                                previous_version_filename = db.get(entity_id).get('filename')
                            db.close()

                            # download the file and updates the History Database
                            if not feed_url is None \
                            and file_fetcher(feed_url, previous_version_filename):
                                db = shelve.open("gtfs-shelve", writeback=True)
                                if db.get(entity_id) == None:
                                    db[entity_id] = {}
                                db[entity_id]['last_known_version'] = feed_version
                                db[entity_id]['filename'] = filename
                                db.close()
                                db = shelve.open("gtfs-shelve")
                                
                                gtfs_fetcher_entry = {
                                    'local_path' : os.path.basename(feed_url),
                                    'ngsi_entity' : entity
                                }
                                plugin_params.append(gtfs_fetcher_entry)
            else:
                print 'Orion Context Broker - invalid response status: {0}'.format(response.status_code)
                print 'Response headers: \n{0}'.format(response.headers)
                print 'Response content: {0}'.format(response.content)
            db.close()
        except:
            print 'Exception trying to get Orion entities: \n{0}\n'.format(request_data)
            print format_exc()

    else:
        print 'Invalid parameter ORION_ENTITY_PATTERNS:{0}'.format(ORION_ENTITY_PATTERNS)
    print "Finished fetching files (%d files fetched)." % len(plugin_params)


    plugin_executer(plugin_params)

    print
    print "Task Finished."

