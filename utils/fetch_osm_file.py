import requests
import sys


if __name__ == "__main__":
    lower_left_lat = 40.6200
    lower_left_lon = -8.8378
    upper_right_lat = 41.5885
    upper_right_lon = -8.0633,
    print(len(sys.argv))
    if len(sys.argv) == 5:
        print("Using custom coordinates")
        lower_left_lat = float(sys.argv[1])
        lower_left_lon = float(sys.argv[2])
        upper_right_lat = float(sys.argv[3])
        upper_right_lon = float(sys.argv[4])
    else:
        print("Using default coordinates")

    response = requests.get("http://overpass-api.de/api/interpreter?data=(node({},{},{},{});<;rel(br););out meta;".format(
        lower_left_lat,
        lower_left_lon,
        upper_right_lat,
        upper_right_lon)
    )

    r = response
    filename = './porto.osm'
    with open(filename, 'wb') as fd:
        for chunk in r.iter_content(chunk_size=128):
            fd.write(chunk)
